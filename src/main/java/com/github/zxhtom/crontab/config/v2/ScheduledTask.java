package com.github.zxhtom.crontab.config.v2;

import java.util.concurrent.ScheduledFuture;

/**
 * @package com.github.zxhtom.crontab.config.v2
 * @Class dsf
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午11:30
 */
public final class ScheduledTask {

    public volatile ScheduledFuture<?> future;
    /**
     * 取消定时任务
     */
    public void cancel() {
        ScheduledFuture<?> future = this.future;
        if (future != null) {
            future.cancel(true);
        }
    }
}
