package com.github.zxhtom.crontab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @package com.github.zxhtom.crontab
 * @Class CrontabApplication
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午9:24
 */
@SpringBootApplication
@EnableScheduling
public class CrontabApplication {
    public static void main(String[] args) {
        SpringApplication.run(CrontabApplication.class, args);
    }
}
