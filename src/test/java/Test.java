import com.github.zxhtom.crontab.CrontabApplication;
import com.github.zxhtom.crontab.mapper.TestMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @package PACKAGE_NAME
 * @Class Test
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午9:42
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CrontabApplication.class)
public class Test {

    @Autowired
    private TestMapper testMapper;

    @org.junit.Test
    public void getTests() {
        List<com.github.zxhtom.crontab.model.Test> tests = testMapper.getTests();
        System.out.println(tests);
    }
}
